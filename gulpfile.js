const {src, dest, parallel, series, watch} = require('gulp');
const browsersync = require('browser-sync').create();

const LOCAL_PORT = 3000;
const PROJECT_DIR = 'sampleApp/';
const START_PATH = 'index.html';

// BrowserSync
function browserSync() {
  browsersync.init({
    port: LOCAL_PORT,
    startPath: START_PATH,
    watch: true,
    server: {
      baseDir: PROJECT_DIR,
    },
  });
}

exports.default = parallel(browserSync);
// exports.dev = parallel(browserSync);
